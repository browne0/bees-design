<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="description" content="BeesDesign is a newly founded web design company that is here to help create online identities for small businesses." />
		<meta name="keywords" content="web design,web development,Bee's Design,bees,design,beesdesign">
		<meta name="author" content"Malik Browne">
		<meta name="viewport" content="width=device-width">
		<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
		<link rel="icon" href="favicon.ico" type="image/x-icon">
		
		<!-- Stylesheets -->
		
		<link rel="stylesheet" type="text/css" href="lib/main.css">
		<link href="lib/lightbox.css" rel="stylesheet" type="text/css" >
		<link rel="stylesheet" href="lib/bootstrap.css">
		<link rel="stylesheet" href="lib/bootstrap-theme.css">
		<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Raleway:700,500' rel='stylesheet' type='text/css'>
		<link href="lib/jquery.bxslider.css" rel="stylesheet">
		<link rel="stylesheet" href="lib/alertify.core.css" >
		<link rel="stylesheet" href="lib/alertify.default.css" >
		
		<!-- Scripts -->
		
		<script src="js/alertify.min.js"></script>
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js">
		</script>
		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBleAVFo38kckNEBExlEMOj2z8a93bTl3I">
	    </script>
	    <script type="text/javascript">
	      function initialize() {
	        var myLatlng = new google.maps.LatLng(41.0338331,-73.7846754);
	        var mapOptions = {
	          center: myLatlng,
	          zoom: 16,
	          panControl: false,
	          mapTypeControl: false,
	          streetViewControl: false,
	          overviewMapControl: false,
	          scrollwheel: false,
	          draggable: false
	        };

	        var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

	        var contentString = "<p class='maptext'>Bee's Design Co.</p><p class='maptext'>White Plains, NY. 10606<br> info@beesdesign.net</p>";

	        var infowindow = new google.maps.InfoWindow({
	      	content: contentString
	  		});


	        var marker = new google.maps.Marker({
		    position: myLatlng,
		    map: map,
		    animation: google.maps.Animation.DROP,
		    title:"Bee's Design Co."
		    });
	        	infowindow.open(map,marker);

	        	google.maps.event.addListener(marker, 'mouseover', function() {
	    		infowindow.open(map,marker);
	  			});

	      }
	      google.maps.event.addDomListener(window, 'load', initialize);
	    </script>

		 <title>Contact | Bee's Design</title>
	</head>

	<body>
	<?php
	function spamcheck($field) {
	$field = filter_var($field, FILTER_SANITIZE_EMAIL);
	if (filter_var($field, FILTER_VALIDATE_EMAIL))
		return TRUE;
	else
		return FALSE;
	}
	?>
		<div id="main">
					<!-- NAVBAR -->
				<nav class="navbar navbar-default navbar-fixed-top">
					<div class="container">
						<div class="navbar-header">
						  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#site-nav">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						  </button>
						  <a class="navbar-brand link" href="http://beesdesign.net"><img src="images/logo.png" alt></a>
						</div>

						<div id="site-nav" class="navbar-collapse collapse">
							<ul class="nav navbar-nav navbar-right">
								<li><a href="about" class="link">About</a></li>
								<li><a href="projects" class="link">Projects</a></li>
								<!--<li><a href="services" class="link">Services</a></li>-->
								<li><a href="contact" class="link">Contact</a></li>
							</ul>
						</div>
					</div>
				</nav>

				<div id="contact">
					<div class="container-fluid contacttitle text-center">
						<h1>Contact</h1>
					</div>
					<div class="container">
						<div class="row">
							<div class="col-sm-6 col-md-6 rowleft">
								<h1>Contact Us</h1>
								<p>We are <b>more</b> than excited to get started on our next project! Could it possibly be yours?</p>
								<p>Feel free to use this form to ask us general questions, or to ask for our opinion on an idea. This is our first step in building a relationship, and you
								should know that Bee's Design is ready to work with <b>YOU</b>.</p>
							</div>
							<div class="col-sm-6 col-md-5 col-md-offset-1">
	<?php
		/*
		 * Global
		 */
		date_default_timezone_set('America/Indianapolis');

		/**
		 * processInput
		 * Process input received from $_POST
		 *
		 * @param array $_POST The $_POST array of input.
		 * @since 1.0
		 */
		function processInput() {
			// Timestamp for later
			$timestamp = date('Y-m-d g:i:s A');

			// Create a boolean gate to prevent us from moving forward w/o checking
			$isInputOK = true;

			// Load English bad word dictionary
			$badwords = file("badwords-en", FILE_IGNORE_NEW_LINES);

			// Chars to remove from string
			$charsToRemove = array(" ", ",");

			// Check if any element in $_POST is inappropriate
			foreach ($_POST as $value) {
				foreach ($badwords as $word) {
					// Strip the chars ($charsToRemove) from the string ($value) and check if it matches a bad word ($word)
					if (strpos(str_replace($charsToRemove, '', $value), $word) !== false) {
						// We have a match
						$isInputOK = false;
						break 2; // break out of both loops to skip checking other words
					}
				}
			}

			// If input wasn't OK, error out. If it was, sent an email.
			if (!$isInputOK) {
				// Bad words detected, throw an error!
				echo "<p>ERROR: Your message contains vulgar words. The police will be sent the following information to track and identify you:\n";
				echo "<ul>\n";
				echo "<li>Your IP: " . $_SERVER['REMOTE_ADDR'] . "</li>\n";
				echo "<li>Your ISP Info: " . gethostbyaddr($_SERVER['REMOTE_ADDR']) . "</li>\n";
				echo "<li>Browser Information: " . $_SERVER['HTTP_USER_AGENT'] . "</li>\n";
				echo "<li>Request Time: " . $timestamp . "</li>\n";
				echo "<li>Your Name, Email, and Message Content</li>\n";
				echo "</ul>\n";
				logMessage($isInputOK, $timestamp);
			} else {
				// No bad words detected, proceed with mail operation
				$subject = "Message from a potential client!: " . $_POST['name'];
				mail("info@beesdesign.net",$subject,$_POST['message'],"From: {$_POST['email']}\n");
				echo "<script type='text/javascript'>alertify.success('Thank you for sending us an e-mail! We will contact you as soon as possible.')</script>";
				echo "<p class='thankyou'>Thank you for sending us an e-mail! <br><br> The information that was sent was: <br> Name: ".$_POST['name']."<br>E-mail: ".$_POST['email']." <br> Message: ".$_POST['message']."</p>";
				logMessage($isInputOK, $timestamp);
			}
		}

		/**
		 * logMessage
		 * Log the submission data in contact-log.txt.
		 *
		 * @param boolean $wasInputOK Whether processInput found bad words or not.
		 * @param string $requestTime The time the form was submitted.
		 * @since 1.0
		 */
		function logMessage($wasInputOK, $requestTime) {
			$prefix = "";
			if ($wasInputOK)
				$prefix = "SUCCESS: Email was sent. ";
			else
				$prefix = "ERROR: Email was not sent. ";

			$event = date('Y-m-d g:i:s A') . "\t" . $prefix .
				"\n\tClientIP='{$_SERVER['REMOTE_ADDR']}',
		\tClientISP='" . gethostbyaddr($_SERVER['REMOTE_ADDR']) . "',
		\tClientBrowser='{$_SERVER['HTTP_USER_AGENT']}',
		\tRequestTime='{$requestTime}',
		\tFormName='{$_POST['name']}',
		\tFormEmail='{$_POST['email']}',
		\tFormMessage='{$_POST['message']}'\r\n";
			$handle = fopen("contact-log.txt", "a");
			fwrite($handle, $event);
			fclose($handle);
		}

		// Process $_POST
		if (isset($_POST["submit"])) {
			processInput();
		} else {
	?>
								<form id="ajax-contact" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
									<div class="field">
										<label for="name">Name</label>
										<input type="text" id="name" class="col-sm-12 input" name="name" placeholder="Your name" required>
									</div>
									<div class="field">
										<label for="email">Email</label>
										<input type="email" id="email" class="col-sm-12 input" name="email" placeholder="Your email" required>
									</div>
									<div class="field">
										<label for="message">Message</label>
										<textarea name="message" id="message" class="col-sm-12" cols="30" rows="10" placeholder="Your message" required></textarea>
									</div>
									<div class="field">
										<button type="submit" name="submit" class="col-sm-12 input">Send Message</button>
									</div>
								</form>
			<?php
	} // endif
	?>
							</div>
						</div>
					</div>
					<div id="map-canvas">
							
					</div>
				</div>
		</div>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
	</body>
</html>