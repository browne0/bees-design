<!DOCTYPE html>
<html lang="en">
<head>
	<title>Bee's Design | Home </title>
	<meta charset="UTF-8">
	<meta name="description" content="BeesDesign is a newly founded web design company that is here to help create online identities for small businesses." />
	<meta name="keywords" content="web design,web development,Bee's Design,bees,design,beesdesign">
	<meta name="author" content"Malik Browne">
	<meta name="viewport" content="width=device-width">
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
	<link rel="icon" href="favicon.ico" type="image/x-icon">
	
	<!-- Stylesheets -->
	
	<link rel="stylesheet" type="text/css" href="lib/main.css">
	<link href="lib/lightbox.css" rel="stylesheet" type="text/css" >
	<link rel="stylesheet" href="lib/bootstrap.css">
	<link rel="stylesheet" href="lib/bootstrap-theme.css">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Raleway:700,500' rel='stylesheet' type='text/css'>
	<link href="lib/jquery.bxslider.css" rel="stylesheet">
	
	<!-- Scripts -->
	
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js">
	</script>
</head>

<body>

  <!--<div id="ibeloading">
    <div id="airplane"><img src="http://beesdesign.net/images/loadingbar.gif" class="bar" alt></div>
  </div>-->

  <div id="main">

            <!--Navigation Bar-->
	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
			  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#site-nav">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>
			  <a class="navbar-brand link" href="http://beesdesign.net"><img src="images/logo.png" alt></a>
			</div>

			<div id="site-nav" class="navbar-collapse collapse">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="about" class="link">About</a></li>
					<li><a href="projects" class="link">Projects</a></li>
					<!--<li><a href="services" class="link">Services</a></li>-->
					<li><a href="contact" class="link">Contact</a></li>
				</ul>
			</div>
		</div>
	</nav>
	
    <div id="home">
		<div class="container">
			<div class="jumbotron">
				<div class="container">
				<h1>Bee's Design.</h1>
					<h2>A simple company that creates online identities.</h2>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-sm-11 col-xs-11 left">
					<h2> Web Design and Development </h2>
					<p>
					We provide client-friendly web solutions, built around web frameworks such as Wordpress, Squarespace, or Twitter's Bootstrap. Our expertise is focused around web design and development, and usability testing.
					</p>
					<p>Let us help you connect to a whole new group of people, with a powerful online presence.</p>
				</div>
				<div class="col-md-4 col-sm-11 col-xs-11 middle">
					<h2> Usability Testing </h2>
					<p>
					Have you ever created a website, but can't find new ways to make your website look better? We can conduct user experience research, and evaluate your website's interface usability to help you create a better product.
					</p>
					<p class="text-center"><a class="contactbtn" href="contact">Contact Us</a></p>
				</div>
				<div class="col-md-4 col-sm-11 col-xs-11 right">
					<h2> Traditional Web Solutions </h2>
					<p>
					At Bee's Design, we also offer more traditional web solutions for our clients, which include:
					</p>
					<ul>
						<li>Website Consulting</li>

						<li>Website Maintenance</li>

						<li>Photography</li>
					</ul>

					<p>All business inquiries:<br>info@beesdesign.net</p>
					
				</div>
			</div>
			<hr></hr>
			<footer><p>&copy; Bee's Design 2015. All rights reserved.</p></footer>
		</div>
	</div><!--End of Home Page-->

	  
      <!--Start of Services Page-->

<!--      <div id="services">

          <!--Content-->

<!--          <div class="wrapper">
		  <div class="container3">
            <p class="serviceshead">
			// OUR MAIN SERVICES
			</p>
			<div class="servicethreewrapper">
			<div class="service">
			<div class="serviceimage">
			<img src="http://placehold.it/300x200" class="servicesmallimg" alt>
			</div>
			<p class="servicetexthead">Web Design</p>
			<p class="servicetext">
			Having a customized website that allows you to reach a target audience that you could never have reached before, is something that is very important. Bee's Design promises to earn your trust, and provide the best experience possible.
			From one page starter sites, to complex sites that are custom designed with database applications, we are here for <b>YOU.</b>
			</p>
			</div>
			<div class="service">
			<div class="serviceimage">
			<img src="http://placehold.it/300x200" class="servicesmallimg" alt>
			</div>
			<p class="servicetexthead">Website Advice</p>
			<p class="servicetext">
			Have you ever created a website, but can't find new ways to make your website look better? Our staff has years of experience, and can provide you with advice to make your website run smoother, or just advice on how to do something specific on your website. Please contact us at info@beesdesign.net for more information on our fees and rates.
			</p>
			</div>
			<div class="service">
			<div class="serviceimage">
			<img src="http://placehold.it/300x200" class="servicesmallimg" alt>
			</div>
			<p class="servicetexthead">Web Maintenance</p>
			<p class="servicetext">
			Our staff here at Bee's Design has worked on websites for a variety of different situations. You will get direct support from our project manager here at Bee's Design to implement things like: Programming, Web Design, Adding a content page to your site, etc. For more information, please make sure to fill out our contact page for rates.
			</p>
			</div>
			</div>
			</div>
            </div>

      </div>
	  
		  <div id="footer">
      <div id="date">
        <script type="text/javascript">
var months = new Array(
        "January", "February", "March", "April",
        "May", "June", "July", "August", "September",
        "October", "November", "December");
        var currentTime = new Date();
        var month = currentTime.getMonth();
        var day = currentTime.getDate();
        var year = currentTime.getFullYear();
        document.write(months[month] + " " + day + ", " + year);
        </script>
      </div>

      <div class="footerright">
        <p class="copyright">Copyright &copy; <script>
document.write(year)
        </script> <i>Bee's Design</i>. All rights reserved.</p>
      </div>
  </div><!--footer-->

 <!--     </div><!--End of contact page-->
<!--main-->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
		<!-- Latest Bootstrap and minified JavaScript -->
  </body>
</html>
